Source: modsecurity-crs
Section: httpd
Priority: optional
Maintainer: Alberto Gonzalez Iniesta <agi@inittab.org>
Uploaders: Ervin Hegedus <airween@gmail.com>
Build-Depends: debhelper (>= 12)
Standards-Version: 4.4.1.1
Homepage: https://coreruleset.org
Vcs-Browser: https://salsa.debian.org/debian/modsecurity-crs
Vcs-Git: https://salsa.debian.org/debian/modsecurity-crs.git

Package: modsecurity-crs
Architecture: all
Depends: ${misc:Depends}
Recommends: libapache2-mod-security2 (>= 2.8.0)
Suggests: lua, geoip-database-contrib, ruby, python
Description: OWASP ModSecurity Core Rule Set
 modsecurity provides critical protections against attacks across most every
 web architecture. CRS is based on generic rules which focus on attack payload
 identification in order to provide protection from zero day and unknown
 vulnerabilities often found in web applications, which are in most cases
 custom coded.
 .
 Core Rules use the following techniques: HTTP request validation, HTTP
 protocol anomalies, Global constraints, HTTP Usage policy, Malicious client
 software detection, Generic Attack Detection (SQL injection, Cross Site
 Scripting, OS Command Injection, ColdFusion, PHP and ASP injection, etc.),
 Trojans & Backdoors Detection, Error Detection, XML Protection, Search Engine
 Monitoring.
